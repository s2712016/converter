package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class CtoFCalculator {
    public double getFahrenheit(String isbn){
        double temperature = Double.parseDouble(isbn) * 1.8 + 32;
        return temperature;
    }
}
